<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>LX AUTOS</title>

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="app\css\style.css">

</head>
<body>

  <!-- Page wrapper -->
  <div id="page-wrapper">

    <!-- Container -->
    <div class="container">

      <!-- Display objects -->
      <div id="object-list"></div>

    </div>

  </div>

  <!-- js -->
  <script src="node_modules\axios\dist\axios.min.js"></script>
  <script src="app\js\objectList.js"></script>

</body>
</html>
