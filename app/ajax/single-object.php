<?php

$curl = curl_init();
$regno = htmlspecialchars($_GET["reg"]);

curl_setopt_array($curl, array(
  CURLOPT_PORT => "81",
  CURLOPT_URL => "http://api.bilonline.se:81/api/",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "apikey=5590-5815-82AB-CDEF&orgnr=559058-1582&ano=true&regnr=" . $regno,
  CURLOPT_HTTPHEADER => array(
    "Cache-Control: no-cache",
    "Content-Type: application/x-www-form-urlencoded"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}

?>
