fetch(url)
.then(response => response.json())
.then(json => {

  if (json.error == null) {
    console.log(json)

    for (let i = 0; i < json.documentList.documents.length; i++) {
      let object = json.documentList.documents[i]

      if (typeof object !== "undefined") {
        if (typeof object.featuredImage !== "undefined") {
          if (typeof object.featuredImage.files[0].formats[0] !== "undefined") {
