const singleObject = () => {

  const getAllUrlParams = (url) => {

    let queryString = url ? url.split('?')[1] : window.location.search.slice(1);
    let obj = {};

    if (queryString) {
      queryString = queryString.split('#')[0];

      let arr = queryString.split('&');

      for (var i=0; i<arr.length; i++) {
        let a = arr[i].split('=');
        let paramNum = undefined;
        let paramName = a[0].replace(/\[\d*\]/, function(v) {
          paramNum = v.slice(1,-1);
          return '';
      });

      let paramValue = typeof(a[1])==='undefined' ? true : a[1];

      if (obj[paramName]) {

        if (typeof obj[paramName] === 'string') {
          obj[paramName] = [obj[paramName]];
        }

        if (typeof paramNum === 'undefined') {
          // put the value on the end of the array
          obj[paramName].push(paramValue);
        }

        else {
          // put the value at that index number
          obj[paramName][paramNum] = paramValue;
        }
      } else {
        obj[paramName] = paramValue;
      }
    }
  }

  return obj;
}

console.log(getAllUrlParams())

  let regz = getAllUrlParams().reg;
  let url = 'http://localhost/lxautos/app/ajax/object-list.php';
  let tmp = ['<div class="row object-wrapper">']
  let errorPic;
  let errorTxt = 'För tillfället kan vi inte visa våra fordon i lager. Vänligen åtekrom lite senare eller titta in på Bytbil eller Blocket :)'

  axios.get(url)
  .then(function (response) {
    console.log(response);
    console.log(dno)

    for (let i = 0; i < response.data.resultat.length; i++) {
      let object = response.data.resultat[i]
      if (object.regnr === null) {

        let objectName = object.mt + ' ' + object.modell;
          let objectDesc = object.modell_b;
          let objectRegnr = object.regnr;
          let objectImg = 'http://bilonline.se/kunder/lxauto/bilder/' + objectRegnr +'/' + objectRegnr +'-1004.jpg'
          objectDesc = objectDesc.substring(0, 25) + '...';

          const objectPrice = () => {
            if (object.NP === NaN) {
              return object.BP
            }
          }

          tmp.push('<div class="object-container col-sm-12 col-md-6 col-lg-4" data-object-id="' + object.ID + '">')
            tmp.push('<a class="object-item object-item--link" href="http://localhost/lxautos/object.php?reg=' + objectRegnr + '">')

              tmp.push('<div class="object-item--image" style="background: url( ' + objectImg + ' ) center center no-repeat; background-size: cover;"></div>')

              tmp.push('<div class="object-item--data">')
                tmp.push('<h3>' + objectName + '</h3>')
                tmp.push('<h5>' + objectDesc + '</h5>')
                tmp.push('<p>' + object.amod + ' / ' + object.vaxel + ' / ' + object.drivm + ' / ' + object.mil + ' mil</p>')
                tmp.push('<div class="object-item--mothprice">')
                  tmp.push('<h3>' + object.Loan_Monthpay + ' kr/mån</h3>')
                tmp.push('</div>')
                tmp.push('<div class="object-item--price">')
                  tmp.push('<h3>' + objectPrice() + ' kr</h3>')
                tmp.push('</div>')
              tmp.push('</div>')

            tmp.push('</a>')
          tmp.push('</div>')

      }
    }
    document.querySelector('#object-list').innerHTML = tmp.join('');
  })

  .catch(function (error) {
    console.log('Shit ain\'t working ' + regz + '');
  })

}
singleObject()
